from flask import Flask, render_template, Response, url_for
import cv2
import sys
import numpy
import landmarks_helper as lh
import dlib
import imutils

app = Flask(__name__)

#use dlibs pre-trained detectors
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

# load index /  home
@app.route('/')
@app.route('/index.html')
def face():
    return render_template('index.html')

# capture the frames to analyze them
def get_frame():

    ramp_frames=100
    # 0 = webcam -> video-url's could be used as well
    camera=cv2.VideoCapture(0) 
    i=1
    while True:
        retval, im = camera.read()
        im = imutils.resize(im, width=400, height=400)
        #find the faces in captured frame
        rects = detector(im, 1)
        # find landmarks and ear / blinks
        lh.addLandmarksandBlinksToImage(im, rects, predictor)
        imgencode=cv2.imencode('.jpg',im)[1]
        # transform the frame 
        stringData=imgencode.tostring()
        yield (b'--frame\r\n'
            b'Content-Type: text/plain\r\n\r\n'+stringData+b'\r\n')
        i+=1

    # closes the webcam, when the video_capture stops
    del(camera)

# calculates the actual face detections, landmarks detections and blink detection
@app.route('/calc')
def calc():
     return Response(get_frame(),mimetype='multipart/x-mixed-replace; boundary=frame')

# starts the application 
if __name__ == '__main__':
    app.run(host='localhost', debug=False, threaded=True)
