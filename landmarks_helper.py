
from collections import OrderedDict
import dlib
import cv2
import pandas as pd
from scipy.spatial import distance as dist
from imutils import face_utils
import numpy as np
import imutils

event = "none"
event2 = "none"

# get the coordinates from the faces
def rect_to_bb(rect):
    x = rect.left()
    y = rect.top()
    w = rect.right() - x
    h = rect.bottom() - y

    return (x,y,w,h)

# map the indexes of the landmarks to each specific face region
FACIAL_LANDMARKS_IDXS = OrderedDict([
	("mouth", (48, 68)),
	("right_eyebrow", (17, 22)),
	("left_eyebrow", (22, 27)),
	("right_eye", (36, 42)),
	("left_eye", (42, 48)),
	("nose", (27, 36)),
	("jaw", (0, 17))
])



# calculate EAR
def turn_aspect_ratio(x1,x2,x3):
	# vertical eye landmarks
	A = dist.euclidean(x1, x2)
	B = dist.euclidean(x2, x3)

	return A/B

# transforms the shape into a NumPy array
def shape_to_np(shape, dtype="int"):
	# initialize the list of (x, y)-coordinates
	coords = np.zeros((68, 2), dtype=dtype)

	# Transforms the 68 landmarks to (x,y) coordiantes
	for i in range(0, 68):
		coords[i] = (shape.part(i).x, shape.part(i).y)
	return coords

def eye_aspect_ratio(eye):
	# compute the euclidean distances between the vertical eye landmarks
	A = dist.euclidean(eye[1], eye[5])
	B = dist.euclidean(eye[2], eye[4])
	C = dist.euclidean(eye[0], eye[3])

	ear = (A + B) / (2.0 * C)
	return ear

def addLandmarksandBlinksToImage(image, rects, predictor):
	#constants + variables for blink detection
	EYE_AR_THRESH = 0.21
	RATIO_THRESH = 0.0017
	EYE_AR_CONSEC_FRAMES = 3
	(lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
	(rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]
	# for every face
	for (i, rect) in enumerate(rects):
		# find face landmarks and convert the
		# (x, y)-coordinates to a NumPy array
		shape = predictor(image, rect)
		shape = face_utils.shape_to_np(shape)

		# extract the left and right eye coordinates to compute the EAR
		leftEye = shape[lStart:lEnd]
		rightEye = shape[rStart:rEnd]
		leftEAR = eye_aspect_ratio(leftEye)
		rightEAR = eye_aspect_ratio(rightEye)
		blink = 0
		
		# average the eye aspect ratio
		ear = (leftEAR + rightEAR) / 2.0
		(x, y, w, h) = face_utils.rect_to_bb(rect)
		ear_ratio = ear/w
		# compute the convex hull for the left and right eye
		leftEyeHull = cv2.convexHull(leftEye)
		rightEyeHull = cv2.convexHull(rightEye)
		cv2.drawContours(image, [leftEyeHull], -1, (0, 255, 0), 1)
		cv2.drawContours(image, [rightEyeHull], -1, (0, 255, 0), 1)

		# check to see if the eye aspect ratio is below the blink
		# threshold, and if so, increment the blink frame counter
		cv2.putText(image, "EAR: {:.5f}".format(ear), (250, 30),
		cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
		if ear_ratio < RATIO_THRESH:
			blink += 1

		# draw facial landmarks
		for (x, y) in shape:
			cv2.circle(image, (x, y), 1, (0, 0, 255), -1)
		
		# turn dlibs rectangle to a specially coloured bounding box
		if(blink<1):
			(x, y, w, h) = face_utils.rect_to_bb(rect)
			cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 255), 2)
			return False
		else:
			(x, y, w, h) = face_utils.rect_to_bb(rect)
			cv2.rectangle(image, (x, y), (x + w, y + h), (124, 252, 0), 2)
			return True
