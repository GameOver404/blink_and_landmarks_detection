# Blink Detection App

- Built using Dlibs shape predictor for the 68 facial landmarks and eye-regions
- With the possibility to retrain it via cvs-files with the help of pandas.
- The Blink Detection was modelled with the help of Adrian Rosebrocks article [Eye blink detection with OpenCV, Python, and dlib](https://www.pyimagesearch.com/2017/04/24/eye-blink-detection-opencv-python-dlib/)


## Prerequisites

This project requires the following dependencies to be installed beforehand:

* Python and pip 
* vs2017 for Windows
* CMake
* xQuartz/X11

## Dependencies

- flask
- dlib
- imutils
- opencv-python
- pandas
- numpy

To install all the needed dependencies run:
```
pip install -r requirements.txt
```

## Usage

- Once the dependencies are installed (via pip) start the app with 
```
python app.py
```