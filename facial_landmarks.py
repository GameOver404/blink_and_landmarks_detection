import dlib
import cv2
import pandas as pd
import landmarks_helper as lh
from imutils import face_utils
import numpy as np
import argparse
import imutils

# dlibs pretrained models
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

#try to open camera
counter = 0
cam = cv2.VideoCapture(0)
if not cam.isOpened():
    raise IOError("Cannot open webcam.")

#if it's successful -> capture frame and start detections
while True:
    ret_val, img = cam.read()

    img = cv2.flip(img, 1)
    img = imutils.resize(img, width=600, height=400)
    rects = detector(img, 1)

    if lh.addLandmarksandBlinksToImage(img, rects, predictor):
        counter+=1

    cv2.putText(img, "Blinks: {}".format(counter), (10, 30),
			cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
        
    cv2.imshow('Output', img)

    if cv2.waitKey(1) == 27: 
        cv2.destroyAllWindows()

# training
#dataset = pd.read_csv('train_data.csv', header=None)

#images = []
#for index, row in dataset.iterrows():
#    image = np.reshape(np.asarray(row), (48, 48))
#    images.append(image)
    #cv2.imwrite('images/img_' + str(index) + '.jpg', image)


