﻿//libraries + class constants
const dlib = require('face-recognition');
const cv = require('opencv4nodejs');
const fr = require('face-recognition')
var counter = 0;

//total number of blinks
const total = 0;
//right and left eye
var left;
var right;


//Eye Detection WITHOUT Blink Detection
function detectEyes(image) {

	const eyeClassifier = new cv.CascadeClassifier(cv.HAAR_EYE);

	// detect eyes
	const faceRegion = image.getRegion(faceRect);
	const eyeResult = eyeClassifier.detectMultiScale(faceRegion);
	console.log('eyeRects:', eyeResult.objects);
	console.log('confidences:', eyeResult.numDetections);

	// get best result
	const eyeRects = sortByNumDetections(eyeResult)
		.slice(0, 2)
		.map(idx => eyeResult.objects[idx]);

	// draw eyes detection in face region
	eyeRects.forEach(eyeRect => drawGreenRect(faceRegion, eyeRect));
	cv.imshowWait('face detection', image);
}

//Eye & Blink Detection
function blinkDetection() {
	/*Precision might be increased, if the following two numbers are
	 changed (depends on the system) */
	EYE_AR_THRESH = 0.3
	EYE_AR_CONSEC_FRAMES = 3

	//Fetch the facial landmarks for each eye
	detector = fr.FrontalFaceDetector();
	predictor = fr.ShapePredictor("./shape_preictor_68_face_landmarks.dat");

	/*The detection also works with one eye without any adjustments.*/
	left = (lStart, lEnd);
	right = (rStart, rEnd);
	left = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"];
	right = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"];
	//Test, if it could be a real face, with Hamming distance
	realFace(left, right);
	//Start the video stream thread
	console.log("[INFO] starting video stream thread...");
	fileStream = true;
	//only for built-in camera / webcam -> for videos use src="URL"
	vs = VideoStream(src=0).start()
	sleep(1000);

	//Preparation for EAR calculation
	gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY);
	rects = detector(gray, 0);

	//EAR is used as an estimate of the eye opening state
	function eye_aspect_ratio(eyes) {

		//compute euclidian distance for each eye (vertical)
		A = dist.euclidean(eye[1], eye[5]);
		B = dist.euclidean(eye[2], eye[4]);

		//compute euclidian distance (horizontal)
		C = dist.euclidean(eye[0], eye[3]);

		//eye_aspect_ratio (EAR formula)
		ear = (A + B) / (2.0 * C);
		return ear;
	}

	//EAR is used as an estimate of the eye opening state
	eye_aspect_ratio(rects);

	for (rect in rects) {
		//determine the facial landmarks for the face region
		shape = predictor(gray, rect);
		shape = face_utils.shape_to_nj(shape);
	}
	/*extract the left and right eye coordinates, then use the
	 coordinates to compute the eye aspect ratio for both eyes */
	leftEye = shape[lStart, lEnd];
	rightEye = shape[rStart, rEnd];
	leftEAR = eye_aspect_ratio(leftEye);
	rightEAR = eye_aspect_ratio(rightEye);

	// average the eye aspect ratio 
	ear = (leftEAR + rightEAR) / 2.0;

	//visualize the eyes
	leftEyeHull = cv.convexHull(leftEye);
	rightEyeHull = cv.convexHull(rightEye);
	cv.drawContours(frame, [leftEyeHull], -1, (0, 255, 0), 1);
	cv.drawContours(frame, [rightEyeHull], -1, (0, 255, 0), 1);

	document.writeln("Bitte sehen Sie bis zum Ablauf des Counters in die Kamera.");
	sleep(1500);
	/*Starts the counter of 30 seconds in order to get the blinks 
	during this time frame*/
	countdown();

	// aspect ratio < blink threshold => increment the blink counter
	if (ear < EYE_AR_THRESH) {
		counter++;
	}
	//otherwise, the eye aspect ratio >= blink threshold
	else {
		/* ‚if the eyes were closed for a sufficient number of
		time, then increment the total number of blinks */
		if (counter >= EYE_AR_CONSEC_FRAMES) {
			total++;
		}
	}
	counter = resetCounter();
	/* draw the total number of blinks on the frame along with
	 the computed eye aspect ratio for the frame */
	cv.putText(frame, "Blinks: {}".format(total), (10, 30),
		cv.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2);
	cv.putText(frame, "EAR: {:.2f}".format(ear), (300, 30),
		cv.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2);

	cv.imshow("Frame", frame);
	countdown();
	key = cv.waitKey(1) && 0xFF;
}

//<p> Bitte sehen Sie noch <span id="countdowntimer">30 </span> Sekunden in die Kamera.</p>
function countdown() {
	var timeleft = 30;
	var timer = setInterval(function () {
		timeleft--;
		document.getElementById("countdowntimer").textContent = timeleft;
		if (timeleft <= 0)
			//clearInterval(timer);
			startLivenessDetection();
	}, 1000);
}

function startLivenessDetection() {
	testLiveness(left, right);
	cv.destroyAllWindows();
	if (testLiveness) {
		//Person is alive

	} else {
		//Person most likely isn't alive 
		alert("You're either a scammer or dead! Neither is good...")
	}
}

//counter is set back to zero 
function resetCounter() {
	counter = 0;
	return counter;
}


/*The counted blinks should be in the given range to ensure that no 
spoofing is taking place. Since the person is looking directly into the camera both eyes 
(or at least their outlines) should be visible, but of course they can't have 
the same values*/

function testLiveness(left, right) {
	alive = false;

	if ((total >= 4 || total <= 21) && left != right && isMoving) {
		alive = true;
	}
	return alive;

}

function isMoving(mark1, mark2) {
	if (mark1 == mark2) {
		return false;
	} else {
		return true;
	}
}

function sleep(ms) {
	var start = new Date().getTime();
	var end = start;
	while (end < (start + ms)) {
		end = new Date().getTime();
	}
}

